#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { StaticWebsiteStack } from '../lib/pimco-poc-stack';


const DOMAIN_NAME = process.env.DOMAIN_NAME;
const S3_BUCKET_NAME = process.env.S3_BUCKET_NAME;
// const BUCKET_NAME = process.env.BUCKET_NAME;
// const FOLDER_NAME = process.env.FOLDER_NAME;
// const INSTANCE_NAME_DEV = process.env.INSTANCE_NAME_DEV;
// const INSTANCE_NAME_PROD = process.env.INSTANCE_NAME_PROD;
// const ALB_NAME = process.env.ALB_NAME;

console.log('Domain Name:', DOMAIN_NAME);
console.log('Bucket Name:', S3_BUCKET_NAME);

if (!DOMAIN_NAME || !S3_BUCKET_NAME) {
  throw new Error('Missing required environment variables');
}

console.log('Domain Name:', DOMAIN_NAME);
console.log('Bucket Name:', S3_BUCKET_NAME);
// console.log('Folder Name:', FOLDER_NAME);
// console.log('Dev Instance:', INSTANCE_NAME_DEV)
// console.log('Prod Instance:', INSTANCE_NAME_PROD)
// console.log('Load Balancer:', ALB_NAME)



interface WebsiteEnvironment {
  domainName: string;        // The domain name of the environment (e.g., "dev", "uat", "prod")
  bucketName: string;        // The name of the S3 bucket associated with the environment
  distributionId?: string; 
  folderName: string;  // Optional: The ID of the CloudFront distribution associated with the environment
}

const devEnvironment: WebsiteEnvironment = {
  domainName: DOMAIN_NAME,
  bucketName: S3_BUCKET_NAME,
  folderName: DOMAIN_NAME
};

// const Environment: WebsiteEnvironment = {
//   DOMAIN_NAME,
//   BUCKET_NAME,
//   FOLDER_NAME,

// };

// console.log('Domain Name:', domainName);
// console.log('Bucket Name:', bucketName);
// console.log('Folder Name:', folderName);

// Define the environment
// const Environment: WebsiteEnvironment = {
//   DOMAIN_NAME,
//   bucketName,
//   folderName,
// };

const app = new cdk.App();
new StaticWebsiteStack(app, 'DevStack', devEnvironment , {
  /* If you don't specify 'env', this stack will be environment-agnostic.
   * Account/Region-dependent features and context lookups will not work,
   * but a single synthesized template can be deployed anywhere. */

  /* Uncomment the next line to specialize this stack for the AWS Account
   * and Region that are implied by the current CLI configuration. */
  env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },

  /* Uncomment the next line if you know exactly what Account and Region you
   * want to deploy the stack to. */
  // env: { account: '123456789012', region: 'us-east-1' },

  /* For more information, see https://docs.aws.amazon.com/cdk/latest/guide/environments.html */
});




//new StaticWebsiteStack(app, 'DevStack', devEnvironment , ;