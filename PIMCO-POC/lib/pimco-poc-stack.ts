import * as cdk from 'aws-cdk-lib';
import * as s3 from 'aws-cdk-lib/aws-s3';
import * as cloudfront from 'aws-cdk-lib/aws-cloudfront';
import * as path from 'path';
import * as fs from 'fs';
import * as s3deploy from 'aws-cdk-lib/aws-s3-deployment';
import * as iam from 'aws-cdk-lib/aws-iam';
import { Construct } from 'constructs';
import { Environment } from 'aws-cdk-lib/aws-appconfig';




interface WebsiteEnvironment {
  domainName: string;        // The domain name of the environment (e.g., "dev", "uat", "prod")
  bucketName: string;        // The name of the S3 bucket associated with the environment
  distributionId?: string;
  folderName: string;         
}

export class StaticWebsiteStack extends cdk.Stack {
  constructor(scope: Construct, id: string, environment: WebsiteEnvironment, props?: cdk.StackProps) {
    super(scope, id, props);

    // Create an S3 bucket for the environment
    const websiteBucket = new s3.Bucket(this, environment.bucketName, {
      bucketName: environment.bucketName,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      websiteIndexDocument: 'index.html',
      websiteErrorDocument: 'error.html',
    //  publicReadAccess: true,
    });
     
    //  // Construct the website endpoint URL
    // const websiteEndpoint = `http://${websiteBucket.bucketRegionalDomainName}/`;
    // console.log('$websiteEndpoint')

    // websiteBucket.addToResourcePolicy(new iam.PolicyStatement({
    //   actions: ['s3:GetObject'],
    //   principals: [new iam.AnyPrincipal()],
    //   resources: [websiteBucket.arnForObjects('*')],
    // }));


    //Create a CloudFront distribution for the environment
    const cloudFrontDistribution = new cloudfront.CloudFrontWebDistribution(this, 'CloudFrontDistribution', {
      originConfigs: [
        {
          s3OriginSource: {
            s3BucketSource: websiteBucket
          },
          behaviors: [{ isDefaultBehavior: true }],
        },
        {
          s3OriginSource: {
            s3BucketSource: websiteBucket,
            // originPath: '/dev', // Set the origin path for the dev folder
          },
          behaviors: [{ pathPattern: '/dev/*' }], // Match requests to /dev/*
        },
        {
          s3OriginSource: {
            s3BucketSource: websiteBucket,
            // originPath: '/uat', // Set the origin path for the uat folder
          },
          behaviors: [{ pathPattern: '/beta/*' }], // Match requests to /uat/*
        },
        {
          s3OriginSource: {
            s3BucketSource: websiteBucket,
            // originPath: '/uat', // Set the origin path for the uat folder
          },
          behaviors: [{ pathPattern: '/staging/*' }], // Match requests to /uat/*
        },
        {
          s3OriginSource: {
            s3BucketSource: websiteBucket,
            // originPath: '/uat', // Set the origin path for the uat folder
          },
          behaviors: [{ pathPattern: '/uat/*' }], // Match requests to /uat/*
        },
        {
          s3OriginSource: {
            s3BucketSource: websiteBucket,
            // originPath: '/prod', // Set the origin path for the prod folder
          },
          behaviors: [{ pathPattern: '/prod/*' }], // Match requests to /prod/*
        },
    
      ],
      viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
    });


    // const cloudFrontDistribution = new cloudfront.CloudFrontWebDistribution(this, 'CloudFrontDistribution', {
    //   originConfigs: [
    //     {
    //       customOriginSource: {
    //         domainName: websiteBucket.bucketWebsiteDomainName,
    //         originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY,
    //       },
    //       behaviors: [{ isDefaultBehavior: true }],
    //     },
    //     {
    //       customOriginSource: {
    //         domainName: websiteBucket.bucketWebsiteDomainName,
    //         originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY,
    //       },
    //       behaviors: [{ pathPattern: '/dev/*' }],
    //     },
    //     {
    //       customOriginSource: {
    //         domainName: websiteBucket.bucketWebsiteDomainName,
    //         originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY,
    //       },
    //       behaviors: [{ pathPattern: '/uat/*' }],
    //     },
    //     {
    //       customOriginSource: {
    //         domainName: websiteBucket.bucketWebsiteDomainName,
    //         originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY,
    //       },
    //       behaviors: [{ pathPattern: '/prod/*' }],
    //     },
    //   ],
    //   viewerProtocolPolicy: cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
    // });

    // Update the distributionId property in the environment object
    environment.distributionId = cloudFrontDistribution.distributionId;

    // Function to create or update static content
    const createOrUpdateStaticContent = (FOLDER_NAME: string) => {
      const contentFolderPath = path.join(__dirname, '../../../dist/angular');
      const indexHtmlPath = path.join(contentFolderPath, 'index.html');
      
      if (!fs.existsSync(contentFolderPath)) {
        console.log(`Folder ${FOLDER_NAME} not found. Creating now...`);
        fs.mkdirSync(contentFolderPath);
      }
      
      if (!fs.existsSync(indexHtmlPath)) {
        console.log(`index.html not found in ${FOLDER_NAME}. Creating now...`);
        // const generatedContent = `<!DOCTYPE html><html><head><title>${folderName} Page</title></head><body><h1>Welcome to ${folderName}!</h1></body></html>`;
        fs.writeFileSync(indexHtmlPath, 'utf-8');
      }
      
      console.log(`Deploying static content from ${FOLDER_NAME} to S3 and CloudFront...`);
      new s3deploy.BucketDeployment(this, `Deploy${environment.domainName}StaticContent${FOLDER_NAME}`, {
        sources: [s3deploy.Source.asset(contentFolderPath)],
        destinationBucket: websiteBucket,
        destinationKeyPrefix: FOLDER_NAME,
        distribution: cloudFrontDistribution,
        distributionPaths: ['/dev/*', '/beta/*', '/staging/*', '/prod/*', '/uat/*'], // Paths to invalidate in the CloudFront distribution
      });
    };


  //   const fs = require('fs');
  //   const path = require('path');
  //   const { execSync } = require('child_process');
  //   const aws = require('aws-sdk');

  //   const createOrUpdateStaticContent = (FOLDER_NAME: string) => {
  //     const contentFolderPath = path.join(__dirname, '../../../src');
  //     const indexHtmlPath = path.join(contentFolderPath, 'index.html');
  //     const s3 = new aws.S3();
  
  //     if (!fs.existsSync(indexHtmlPath)) {
  //         console.error(`index.html not found in src folder.`);
  //         return;
  //     }
  
  //     console.log(`Deploying static content from src to ${FOLDER_NAME} folder in S3 and CloudFront...`);
  
  //     s3.putObject({
  //         Bucket: environment.bucketName,
  //         Key: `${FOLDER_NAME}/index.html`,
  //         Body: fs.readFileSync(indexHtmlPath),
  //         ContentType: 'text/html',
  //     }, (err: Error | null, data: any) => {
  //         if (err) {
  //             console.error(`Error uploading index.html to S3: ${err}`);
  //         } else {
  //             console.log(`Uploaded index.html to ${FOLDER_NAME} folder in S3.`);
  //         }
  //     });
  // };
      

    // Create or update static content for dev, uat, and prod
    createOrUpdateStaticContent(environment.domainName);
    // createOrUpdateStaticContent('uat');
    // createOrUpdateStaticContent('prod');


    

    // Output the CloudFront domain name for reference
    new cdk.CfnOutput(this, `${environment.domainName}CloudFrontDomain`, {
      value: cloudFrontDistribution.distributionDomainName,
    });
  }
}

// Define the dev environment
// const devEnvironment: WebsiteEnvironment = {
//   domainName: 'dev',
//   bucketName: 'fundetails',
//   folderName: 'dev'
// };
















// const createOrUpdateStaticContent = (folderName: string) => {
//   const contentFolderPath = path.join(__dirname, '../../', folderName);
//   const indexHtmlPath = path.join(contentFolderPath, 'index.html');
  
//   if (!fs.existsSync(contentFolderPath)) {
//     console.log(`Folder ${folderName} not found. Creating now...`);
//     fs.mkdirSync(contentFolderPath);
//   }
  
//   if (!fs.existsSync(indexHtmlPath)) {
//     console.log(`index.html not found in ${folderName}. Creating now...`);
   
//     fs.writeFileSync(indexHtmlPath, 'utf-8');
//   }